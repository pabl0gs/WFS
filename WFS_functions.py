import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sfs
import wave
import scipy.io.wavfile
import sys
import warnings

#Catch and ignore warnings
warnings.filterwarnings(action="ignore", category=RuntimeWarning)
warnings.filterwarnings(action="ignore", category=np.ComplexWarning)

c= 340 #Sound speed [m/s]

def df_calculation(x, N, Fs, dist_ls, num_ls, source_position, analysis_position, array_position, room_dimensions, result_file):
    """Computes the driving functions"""

    #CALCULATED VALUES
    Ts = 1/Fs
    f = np.linspace(0, Fs / 2, num = N + 1, endpoint=True) #Frequency vector
    w = 2 * np.pi * f #Angular frequency vector [rad/s]
    k = w / c #Number of wave vector [1/m]
    num_input_blocks = np.ceil(np.size(x) / N).astype(int) #Number of blocks to process
    x = zero_padding(x, N * (num_input_blocks + 1)) #Zero padding for avoiding block-parity errors in Overlap-Adding
    output_size = len(x) #Length of each driving function


    #PHYSICAL PARAMETERS OF THE INSTALLATION
    ls_position = np.zeros((num_ls, 3)) #Array with the coordenates of each loudspeaker
    ls_angle = np.zeros((num_ls, 1)) #Array with the θ angles between each loudspeaker and the virtual source [rads]
    dist = np.zeros((num_ls, 1)) #Euclidean distance between the virtual source and each loudspeaker
    x_init = (room_dimensions[0] - (dist_ls * (num_ls - 1))) / 2 #'x' coordenate of the 1st loudspeaker for making the array centered 
    for i in range(num_ls): 
        ls_position[i]=(x_init + i * dist_ls, array_position, 0)
    s0 = array_position - source_position[1] #'y' distance between the virtual source and the loudspeaker array
    r0 = analysis_position[1] - array_position #'y' distance between the analysis point and the loudspeaker array
    for i in range(num_ls): 
        dist[i] = np.sqrt(np.sum(np.square(ls_position[i] - source_position))) 
        ls_angle[i] = np.arccos(s0 / dist[i])  
    mid_position_index = int(len(ls_position) / 2) #For calculating the center of the array
    if num_ls % 2 == 0: #Pair array
        mid_position = [ls_position[mid_position_index][0] + dist_ls / 2, array_position, 0]
    else: #Odd array
        mid_position = [ls_position[mid_position_index][0] + dist_ls, array_position, 0] 
 

    #CALCULATION OF THE DRIVING FUNCTIONS
    A = np.zeros((N + 1, num_ls), dtype = 'complex_') #Amplitude factor of the driving functions
    retardos = np.zeros((N + 1, num_ls), dtype = 'complex_')
    Q = np.zeros((N + 1, num_ls), dtype = 'complex_')
    q = np.zeros((int(2*N), num_ls), dtype = 'complex_')
    previous_frame = np.zeros((int(2*N), num_ls), dtype = 'complex_')
    driving_functions = np.zeros((output_size, num_ls), dtype = 'complex_') #Driving functions
    window = np.bartlett(2*N) #Triangular window
    for n in range(num_input_blocks):
        x_frame = x[n*N:(n+1)*N + N] #Nth input block extraction
        X_frame = np.fft.rfft(x_frame) #Fourier Transformation (FFT) but only with positive values (RFFT)
        for i in range(num_ls):
            A[:, i] = (np.cos(ls_angle[i]) / dist[i]) * np.sqrt(r0 / (s0 + r0)) * np.sqrt((1j * k) / (2 * np.pi)) #Terms of amplitude
            retardos[:, i] = np.exp(-1j * k * dist[i]) #Delays
            Q[:, i] = X_frame * A[:, i] * retardos[:, i] #Driving functions
            q[:, i] = np.fft.irfft(Q[:, i]) #Back to time domain
            q[:, i] = window *  q[:, i]
            driving_functions[n * N : n * N + int(len(q[:, i]) / 2), i] = q[0 : int(len(q[:, i]) / 2), i] + previous_frame[int(len(q[:, i]) / 2) : int(len(q[:, i])) , i] #Overlap-Add
            previous_frame[:, i] = q[:, i]

    driving_functions = driving_functions * 0.4 #Scaling factor for not saturating
    driving_functions = driving_functions.astype(np.int16) #Data type adjustments (pops ComplexWarning)
    return driving_functions, output_size, ls_position, mid_position

def zero_padding(x, new_length):
    """Fills a funciton with 0s"""
    output = np.zeros((new_length,))
    output[:x.shape[0]] = x
    return output

def limit_freq(dist_ls):
    """Displays the limit frequency of the system"""
    f_nyq = c / (2 * dist_ls)
    print('The system has a limit frequency of ', int(f_nyq), 'Hz')


def read_wav(source_file):
    """Reads the WAV file and its parameters"""
    Fs, x = scipy.io.wavfile.read(source_file) #Audio file reading
    x_wave = wave.open(source_file, "r") #Mono-stereo check
    if x_wave.getnchannels() > 1: #If the file is multichannel aborts the execution
        print('You have selected a multichannel file. Please choose a mono file')
        sys.exit()
    return Fs, x  


def save_wav(driving_functions, result_file, Fs, N, num_ls, output_size, save_stereo_files):
    """Reads the WAV file, structures it and saves the driving functions"""
    wv = wave.open(result_file, 'w') #File creation
    wv.setparams((num_ls, 2, Fs, N, 'NONE', 'not compressed')) #num_ls channels, 2 bytes/frame (can be modified)
    wv.close()
    output_file = scipy.io.wavfile.write(result_file, Fs, driving_functions) #Writing

    #Exporting also in separated stereo files
    if save_stereo_files == True:
        for i in range(num_ls):
            if i %2 == 0:
                wv = wave.open(result_file, 'w') #File creation
                wv.setparams((2, 2, Fs, N, 'NONE', 'not compressed')) #2 channels (stereo)
                wv.close()
                output_file = scipy.io.wavfile.write(str(i + 1) + '&' + str(i+2) + '_' + result_file, Fs, driving_functions[:, (i, i+1)]) #Writing CHECKEAR SI ESTÁ BIEN


def room_plots(f, num_ls, dist_ls, ls_position, source_position, room_dimensions, mid_position):
    """Plots the room and the sound distribution in it"""
    if num_ls <= 1: 
        print('It is imposible to implement a single speaker WFS installation')
        sys.exit()
    
    w = 2 * np.pi * f
    xnorm = [1, 1, 0]  # Normalization point for plots
    tapering = sfs.tapering.tukey  #Tapering window
    talpha = 0.3  #Parameter for tapering window
    pw_angle = 90  #Direction of propagation of the plane wave
    grid = sfs.util.xyz_grid([0, room_dimensions[0]], [0, room_dimensions[1]], 0, spacing=0.02) #Dimensions to be ploted
    anormal = sfs.util.direction_vector(np.radians(90), np.radians(90)) #Orientation along the 'x' axis
    array  = sfs.array.linear(num_ls, dist_ls, center = mid_position, orientation = anormal) #For obtaining 'array.n', speaker orientation vector
    d, selection, secondary_source = sfs.fd.wfs.line_2d(w, array.x, array.n, source_position)
    twin = tapering(selection, alpha=talpha)
    p = sfs.fd.synthesize(d, twin, array, secondary_source, grid=grid) #Pops RuntimeWarning
    npw = sfs.util.direction_vector(np.radians(pw_angle), np.radians(90)) #Direction of the "arrow" 

    #Plot room and soundfield
    fig, ax = plt.subplots()
    room = patches.Rectangle((0, 0), room_dimensions[0], room_dimensions[1], linewidth = 3, edgecolor = 'black', facecolor = 'none') #Rectangle with the shape of the room
    ax.add_patch(room)
    sfs.plot2d.amplitude(p, grid, xnorm=xnorm)
    sfs.plot2d.loudspeakers(ls_position, array.n)
    sfs.plot2d.virtualsource(source_position)
    sfs.plot2d.virtualsource([1, 1], npw, type='plane') #"Arrow"
    plt.grid() 
    plt.title('Sound distribution in the room for f = %i Hz' %f, fontsize = 10)
    #plt.xlim(-1, room_dimensions[0] + 1) #Standard plotting
    #plt.ylim(-1, room_dimensions[1] + 1)
    plt.xlim(room_dimensions[0] + 1, -1) #Mirror plotting (adjusts to what happens in reality)
    plt.ylim(room_dimensions[1] + 1, -1)
    


def time_plots(x, driving_functions, num_ls, Fs, N):
    """Plots the signals in the time domain"""
    Ts = 1 / Fs
    x = zero_padding(x, len(driving_functions)) #x length adjustment to match the df
    t = np.linspace(0, (len(x) - 1) * Ts, len(x)) #Time axis 
    fig, axs = plt.subplots(num_ls+1,sharex = True, sharey= True)
    fig.suptitle('Signals in the time domain')
    axs[0].plot(t, x)
    axs[0].set_title('Input signal')
    axs[1].set_title('Driving functions')
    axs[0].set_ylabel('Amplitude')
    for i in range(num_ls):
        axs[i+1].plot(t, driving_functions[:, i], color = "green")
        axs[i + 1].set_ylabel('Amplitude')
    axs[i + 1].set_xlabel('Time [s]')
    


def freq_plots(x, driving_functions, num_ls, Fs, N):
    """Plots the signals in the frequency domain"""
    x = zero_padding(x, len(driving_functions)) #x length adjustment to match the df
    f = (Fs/len(x)) * np.arange(0, len(x)//2) #Frequency axis
    X = np.fft.fft(x)
    X = abs(X[0:len(x)//2]) #Negative axis values ​​are removed from the FFT
    DF = np.zeros((len(X), num_ls))
    fig, axs = plt.subplots(num_ls+1,sharex = True, sharey= True)
    fig.suptitle('Signals in the frequency domain')
    axs[0].plot(f, X)
    axs[0].set_title('Input signal')
    axs[1].set_title('Driving functions')
    axs[0].set_ylabel('FFT spectre')
    for i in range(num_ls):
        DF_fft = np.fft.fft(driving_functions[:, i])
        DF[:, i] = abs(DF_fft[0:len(x)//2])
        axs[i+1].plot(f, abs(DF[:, i]), color = "green")
        axs[i + 1].set_ylabel('FFT spectre')
    axs[i + 1].set_xlabel('Frequency [Hz]')
    axs[0].set_xlim(20, 20000)
    axs[0].set_xscale('log')


