from WFS_functions import *
import matplotlib.pyplot as plt


#INPUT PARAMETERS
N = 16384 #Length of the processing block [samples]
dist_ls = 0.17 #Distance between loudspeakers [m] 
num_ls = 6  #Number of loudspeakers
source_position = [5.5, 1, 0] #Coordenates of the virtual source [x,y,z]
analysis_position = [5, 5, 0] #Coordenates of the analysis point [x,y,z]
array_position = 2 # 'y' coordenate where the loudspeaker array is located [m]
room_dimensions = [10, 5] #Room dimensions [x,y]
source_file = r'name-and-location-of-audio-file.wav' #Name and location of the source WAV file
result_file = 'Driving functions.wav' #Name of the WAV output file with the Driving Functions 


#INPUT SIGNAL
Fs, x = read_wav(source_file)

#CALCULATION AND PROCESSING OF THE DRIVING FUNCTIONS
driving_functions, output_size, ls_position,  mid_position = df_calculation(x, N, Fs, dist_ls, num_ls, source_position, analysis_position, array_position, room_dimensions, result_file)
limit_freq(dist_ls) 

#AUDIO SAVING
save_stereo_files = True #Allows to export the driving functions in separated stereo files
save_wav(driving_functions, result_file, Fs, N, num_ls, output_size, save_stereo_files)    

#SIGNAL PLOTS
time_plots(x, driving_functions, num_ls, Fs, N)
freq_plots(x, driving_functions, num_ls, Fs, N) 

#ROOM PLOTS & PARAMETERS
f = 1000 #Frequency to plot
room_plots(f, num_ls, dist_ls, ls_position, source_position, room_dimensions, mid_position) 
plt.show()




